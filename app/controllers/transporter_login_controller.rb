require 'net/http'
require 'uri'
require 'json'
class TransporterLoginController < ApplicationController
    def index
        @tlogin = ""
    end     
    
    def create
        @userId = params[:username]
        @pwd = params[:password]
        $userRole_frm = params[:hiddenType]
         begin
            uri = URI('http://gas-poc4.cfapps.io/GAS/services/login/')
            http = Net::HTTP.new(uri.host, uri.port)
            req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
            req.body = {"user_name" => @userId, "pwd" => @pwd,"user_role" => $userRole_frm}.to_json
            res = http.request(req)
            data_hash = JSON.parse(res.body)
            puts data_hash["status"]
            $user_id_transporter = "All"
           if data_hash["status"] == "Success"
                redirect_to :controller => "nominations", :action => "index"   
           end
            
            
        end        
     end
end
