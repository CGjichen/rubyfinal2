Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    resources :shipper_login, :transporter_login, :nominations
    
    root "shipper_login#index"
     
    get "/shipper", to: "shipper_login#index"
    get "/transporter", to: "transporter_login#index"
   get "/nominations/schedule", to: "nominations#show" 
end
