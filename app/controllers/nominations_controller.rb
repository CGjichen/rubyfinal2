require 'net/http'
require 'uri'
require 'json'
class NominationsController < ApplicationController
       
    def index
        if($userRole_frm == "Supplier")
            @user_id = $user_id_supplier
        elsif($userRole_frm == "Transporter")
            @user_id = $user_id_transporter 
        end
      
        uri = URI('http://gas-poc4.cfapps.io/GAS/services/viewnomination/')
        http = Net::HTTP.new(uri.host, uri.port)
        req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
        req.body = {"userid" => @user_id}.to_json
        res = http.request(req)
        hash_response  = JSON.parse(res.body)
        @nominations = hash_response         
        respond_to do |format|
          format.html  
         format.json  { render :json => @nominations }
        end
    end
    
   def new
        @nomination = ""
        @status = params[:status]
        @capacityRef = params[:capacity_ref]
        @fname = params[:fname]
        @startdate_and_time = params[:startdate_and_time]
        @enddate_and_time = params[:enddate_and_time]
        @injection_point = params[:injection_point]
        @injection_quantity = params[:injection_quantity]
        @offtake_point = params[:offtake_point]
        @offtake_quantity = params[:offtake_quantity]
        @nomination_type = params[:nomination_type]
        @statusNomination = params[:statusNomination]
        @nominationid = params[:nominationid]

   end
    
                                 
    def show
        @window = params[:id]
        if(@window == "notification")
            json_response  = File.read("#{Rails.root}/mockJsonNotify.json")
            hash_response  = JSON.parse(json_response )
            @notifications = hash_response         
            respond_to do |format|
               format.html  {render "nominations/notification"}
               format.json  { render :json => @notifications }
            end
        elsif(@window == "schedule")
             begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/viewscheduler/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = { "userid" => "All"}.to_json 
                res = http.request(req)
                hash_response  = JSON.parse(res.body)
                @schedules = hash_response   
                respond_to do |format|
                    format.html  {render "nominations/schedule"}
                    format.json  { render :json => @schedules }
                end
            end
        end
     end
    
    def create
        if params[:hiddenStatus] == "Create"
            begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/addnomination/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = { "startdate_and_time" => params[:nominationstartdate],
                            "enddate_and_time" => params[:nominationenddate],
                            "injection_point" => params[:injectionpoint],
                            "injection_quantity" => params[:injectionquantity].to_i,
                            "offtake_point" => params[:offtakepoint],
                            "offtake_quantity" => params[:offtakequantity].to_i,
                            "capacity_ref" => params[:capacity],
                            "status" => "Accepted",
                            "uid" => ($user_id_supplier.to_i)}.to_json 
                res = http.request(req)
                data_hash = JSON.parse(res.body)
                redirect_to :controller => "nominations", :action => "index"  
            end
        elsif params[:hiddenStatus] == "Modify"
            begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/modifynomination/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = {"nominationid" =>params[:hiddenNominationId].to_i,
                            "startdate_and_time" => params[:nominationstartdate],
                            "enddate_and_time" => params[:nominationenddate],
                            "injection_point" => params[:injectionpoint],
                            "injection_quantity" => params[:injectionquantity].to_i,
                            "offtake_point" => params[:offtakepoint],
                            "offtake_quantity" => params[:offtakequantity].to_i,
                            "capacity_ref" => params[:capacity]}.to_json 
                res = http.request(req)
                data_hash = JSON.parse(res.body)
                redirect_to :controller => "nominations", :action => "index"  
            end
           
        end
        
    end       
end
